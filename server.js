const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const notes = require('./routes/api/notes');

const app = express();

//Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

app.get('/', (req, res) => {
  res.send('<h1>Notes</h1>');
});

//Use routes
app.use('/api/notes', notes);

const port = process.env.PORT || 9001;

app.listen(port, () => console.log(`Server running on port ${port}`));
