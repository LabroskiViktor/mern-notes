const express = require('express');
const router = express.Router();

//Load notes controller
const notes = require('../../controllers/notes');

/**
 * @route GET api/notes
 * @desc  Get all notes
 * @access Public
 */
router.get('/', notes.getAllNotes);

/**
 * @route GET api/notes/:id
 * @desc  Get single note
 * @access Public
 */
router.get('/:id', notes.getSingleNote);

/**
 * @route POST api/notes
 * @desc  Create note
 * @access Public
 */
router.post('/', notes.createNote);

/**
 * @route PUT api/notes/:id
 * @desc  Update note
 * @access Public
 */
router.put('/:id', notes.updateNote);

/**
 * @route DELETE api/notes/:id
 * @desc  Delete note
 * @access Public
 */
router.delete('/:id', notes.deleteNote);

module.exports = router;
