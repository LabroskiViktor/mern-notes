import axios from 'axios';
import { GET_NOTES, ADD_NOTE, DELETE_NOTE } from './types';

export const getNotes = () => dispatch => {
  axios
    .get('/api/notes')
    .then(result =>
      dispatch({
        type: GET_NOTES,
        payload: result.data
      })
    )
    .catch(error => console.log(error));
};

export const addNote = note => dispatch => {
  axios
    .post('/api/notes', note)
    .then(result =>
      dispatch({
        type: ADD_NOTE,
        payload: result.data
      })
    )
    .catch(error => console.log(error));
};

export const deleteNote = id => dispatch => {
  axios
    .delete(`/api/notes/${id}`)
    .then(result =>
      dispatch({
        type: DELETE_NOTE,
        payload: id
      })
    )
    .catch(error => console.log(error));
};
