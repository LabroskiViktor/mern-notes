import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Notes from './components/Notes';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Notes />
      </Provider>
    );
  }
}

export default App;
