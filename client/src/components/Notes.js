import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getNotes } from '../actions/noteActions';
import { withStyles } from '@material-ui/core/styles';
import NoteItem from './NoteItem';
import NoteForm from './NoteForm';
import Loading from './Loading';

const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    padding: `0 ${theme.spacing.unit * 3}px`
  }
});

class Notes extends Component {
  componentDidMount() {
    this.props.getNotes();
  }

  componentWillReceiveProps() {
    this.props.getNotes();
  }

  render() {
    const { notes } = this.props.notes;
    const { classes } = this.props;

    let notesContent;

    notesContent =
      notes.length !== 0 ? <NoteItem notes={notes} /> : <Loading />;

    return (
      <div className={classes}>
        <NoteForm /> <br />
        {notesContent}
      </div>
    );
  }
}

Notes.propTypes = {
  notes: PropTypes.object.isRequired,
  getNotes: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  notes: state.note
});

export default connect(
  mapStateToProps,
  { getNotes }
)(withStyles(styles)(Notes));
