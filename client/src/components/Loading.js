import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import '../App.css';
const Loading = () => {
  return (
    <div className="flex-container">
      <CircularProgress disableShrink />
    </div>
  );
};

export default Loading;
