import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteNote } from '../actions/noteActions';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = theme => ({
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing.unit}px auto`,
    padding: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: 'none'
  },
  margin: {
    margin: theme.spacing.unit * 2
  },
  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`
  }
});

class NoteItem extends Component {
  onDeleteClick(id) {
    this.props.deleteNote(id);
  }

  render() {
    const { notes, classes } = this.props;
    const noteItems = notes.map(note => (
      <Paper className={classes.paper} key={note._id}>
        <Grid container wrap="nowrap">
          <Grid item xs={10} md={10}>
            <Typography>{note.text}</Typography>
          </Grid>
          <Grid item xs={2} md={2}>
            <IconButton
              className={classes.button}
              aria-label="Delete"
              color="primary"
            >
              <DeleteIcon
                fontSize="small"
                onClick={this.onDeleteClick.bind(this, note._id)}
              />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    ));
    return <div>{noteItems}</div>;
  }
}

NoteItem.propTypes = {
  deleteNote: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
};

export default connect(
  null,
  { deleteNote }
)(withStyles(styles)(NoteItem));
