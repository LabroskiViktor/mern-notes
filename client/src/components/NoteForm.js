import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { addNote } from '../actions/noteActions';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing.unit}px auto`,
    padding: theme.spacing.unit * 2
  }
});

class NoteForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const newNote = {
      text: this.state.text
    };
    this.props.addNote(newNote);
    this.setState({ text: '' });
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={16}>
          <Grid item xs>
            <form onSubmit={this.onSubmit}>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="text">New Note</InputLabel>
                <Input
                  name="text"
                  type="text"
                  id="password"
                  value={this.state.text}
                  onChange={this.onChange}
                />
              </FormControl>
            </form>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

NoteForm.propTypes = {
  addNote: PropTypes.func.isRequired
};

export default connect(
  null,
  { addNote }
)(withStyles(styles)(NoteForm));
