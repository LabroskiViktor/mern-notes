const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema

const NoteSchema = new Schema({
  text: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Note = mongoose.model('notes', NoteSchema);
