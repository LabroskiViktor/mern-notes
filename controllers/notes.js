const Note = require('../models/Note');

/**
 * @desc Get all notes
 * @param {object} req
 * @param {object} res
 */
const getAllNotes = (req, res) => {
  Note.find()
    .sort({ date: -1 })
    .then(notes => res.json(notes))
    .catch(error => res.status(404).json({ error: 'Not Found' }));
};

/**
 * @desc Get single note
 * @param {object} req
 * @param {object} res
 */
const getSingleNote = (req, res) => {
  Note.findById(req.params.id)
    .then(note => res.json(note))
    .catch(error => res.status(404).json({ error: 'Note not found' }));
};

/**
 * @desc Create new note
 * @param {object} req
 * @param {object} res
 */
const createNote = (req, res) => {
  const newNote = new Note({
    text: req.body.text
  });

  newNote
    .save()
    .then(note => res.json(note))
    .catch(error => res.status(400).json({ error: 'Something went wrong' }));
};

/**
 * @desc Update note
 * @param {object} req
 * @param {object} res
 */
const updateNote = (req, res) => {
  const updateNote = {
    text: req.body.text
  };
  Note.findOneAndUpdate(
    { _id: req.params.id },
    { $set: updateNote },
    { new: true }
  )
    .then(note => res.json(note))
    .catch(error => res.status(400).json({ error: 'Something went wrong' }));
};

/**
 * @desc Dele note
 * @param {object} req
 * @param {object} res
 */
const deleteNote = (req, res) => {
  Note.findById(req.params.id)
    .then(note => {
      note
        .remove()
        .then(() => res.json({ success: true }))
        .catch(error =>
          res.status(400).json({ error: 'Something went wrong' })
        );
    })
    .catch(error => res.status(404).json({ error: 'Note not found' }));
};

module.exports = {
  getAllNotes,
  createNote,
  updateNote,
  deleteNote,
  getSingleNote
};
